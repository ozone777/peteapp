//
//  main.m
//  HelloWorld2
//
//  Created by Pedro Consuegra on 8/14/18.
//  Copyright © 2018 wordpressPete. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <AppleScriptObjC/AppleScriptObjC.h>

int main(int argc, const char * argv[]) {
    [[NSBundle mainBundle] loadAppleScriptObjectiveCScripts];
    return NSApplicationMain(argc, argv);
}
