--
--  AppDelegate.applescript
--  HelloWorld2
--
--  Created by Pedro Consuegra on 8/14/18.
--  Copyright © 2018 wordpressPete. All rights reserved.
--

script AppDelegate
	property parent : class "NSObject"
    
	-- IBOutlets
	property theWindow : missing value
    
    #property theTextField : missing value
    property theComboBox : missing value
    property theApacheSw : missing value
    property theDatabaseSw : missing value
    property theDnsmasqSw : missing value
    property thePHPversion : missing value
    property theOutput : missing value
	
	on applicationWillFinishLaunching_(aNotification)
        
        theOutput's setString_("WordPress efficiency starts here.")
        
        #Get the PHP active version
        set phpv to do shell script "export PATH=/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:$PATH; php -r 'echo phpversion();' || echo 'Off'"
        
        if ((phpv as string) is not equal to "Off") then
            #set phpnumber to text 1 thru 3 of phpv
            #set phpfinal to "php "&phpnumber
            #set theComboBox's stringValue to phpfinal
            set thePHPversion's stringValue to phpv
        else
            theOutput's setString_("PHP Load error")
        end if
        
        #Get MariaDB status
        delay 5
        set dbStatus to do shell script "export PATH=/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:$PATH; mysqladmin version | grep Uptime || echo 'Off'"
        
        if ((dbStatus as string) is not equal to "Off") then
            set dbStatus to text 1 thru 6 of dbStatus
        end if
        
        if ((dbStatus as string) is equal to "Uptime") then
            set dbStatus to "On"
        else
            set dbStatus to "Off"
        end if
        
        set theDatabaseSw's stringValue to dbStatus
        
        #Get Apache status
        set apacheStatus to do shell script "curl -I http://pete.test --max-time 5 | grep Location || echo 'Off'"
        log apacheStatus
        
        if ((apacheStatus as string) is equal to  "Location: http://pete.test/auth/login") then
            set apacheStatus to "On"
            set dnsmasqStatus to "On"
        else
            set apacheStatus to "Off"
            set dnsmasqStatus to "Off"
        end if
        
        set theApacheSw's stringValue to apacheStatus
        set theDnsmasqSw's stringValue to dnsmasqStatus
        
	end applicationWillFinishLaunching_
    
    on applicationWillBecomeActive_(aNotification)
        
        set db_current to ""
        set db_current to theDatabaseSw's stringValue() as text
    
            
        #Get MariaDB status
        set dbStatus to do shell script "export PATH=/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:$PATH; mysqladmin version -u root | grep Uptime || echo 'Off'"
            
        if ((dbStatus as string) is not equal to "Off") then
            set dbStatus to text 1 thru 6 of dbStatus
        end if
            
        if ((dbStatus as string) is equal to "Uptime") then
            set dbStatus to "On"
        else
            set dbStatus to "Off"
        end if
        set theDatabaseSw's stringValue to dbStatus
            
        set apache_current to ""
        set apache_current to theApacheSw's stringValue() as text
        
        #Get Apache status
        set apacheStatus to do shell script "curl -I http://pete.test --max-time 5 | grep Location || echo 'Off'"
        log apacheStatus
            
        if ((apacheStatus as string) is equal to  "Location: http://pete.test/auth/login") then
            set apacheStatus to "On"
            set dnsmasqStatus to "On"
        else
            set apacheStatus to "Off"
            set dnsmasqStatus to "Off"
        end if
            
        set theApacheSw's stringValue to apacheStatus
        set theDnsmasqSw's stringValue to dnsmasqStatus
        
    end applicationWillBecomeActive_
    
    
	on applicationShouldTerminate_(sender)
		-- Insert code here to do any housekeeping before your application quits 
		return current application's NSTerminateNow
	end applicationShouldTerminate_
    
    on append_to_output(textView,stringToAppend)
        set textStorage to textView's textStorage()
        set theText to textStorage's |string|()
        set finaltext to theText as string &"\n"&stringToAppend
        theOutput's setString_(finaltext)
    end append_to_output
    
    on buttonStart_(sender)
        
        set outputTxt to do shell script "sudo /usr/sbin/apachectl stop" with administrator privileges
        if ((outputTxt as string) is not equal to "") then
            append_to_output(theOutput,outputTxt)
        end if
        
        set outputTxt to do shell script "sudo apachectl stop" with administrator privileges
        if ((outputTxt as string) is not equal to "") then
            append_to_output(theOutput,outputTxt)
        end if
       
        set outputTxt to do shell script "sudo launchctl unload -w /System/Library/LaunchDaemons/org.apache.httpd.plist 2>/dev/null" with administrator privileges
        if ((outputTxt as string) is not equal to "") then
            append_to_output(theOutput,outputTxt)
        end if
        
        set outputTxt to do shell script "sudo launchctl load -w /Library/LaunchDaemons/pete.mxcl.httpd.plist" with administrator privileges
        if ((outputTxt as string) is not equal to "") then
            append_to_output(theOutput,outputTxt)
        end if
       
        delay 3
        
        set outputTxt to do shell script "export PATH=/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:$PATH; sudo apachectl start" with administrator privileges
        if ((outputTxt as string) is equal to "") then
            set outputTxt to "Apache started OK"
        end if
        append_to_output(theOutput,outputTxt)
        
        set outputTxt to do shell script "export PATH=/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:$PATH; brew services start mysql@5.7 || echo 'unable to start mysql'"
        append_to_output(theOutput,outputTxt)
        
        set outputTxt to do shell script "export PATH=/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:$PATH; sudo brew services start dnsmasq" with administrator privileges
        append_to_output(theOutput,outputTxt)
        
        set theApacheSw's stringValue to "On"
        set theDatabaseSw's stringValue to "On"
        set theDnsmasqSw's stringValue to "On"
        
        delay 5
        
        open location "http://pete.test"
        
        tell application "Safari"
            open location "http://pete.test"
            set docUrl to URL of document 1
            
            set URL of document 1 to docUrl
        end tell
        
    end buttonStart_
    
    on buttonStop_(sender)
        
        set outputTxt to do shell script "export PATH=/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:$PATH; sudo apachectl stop" with administrator privileges
        if ((outputTxt as string) is equal to "") then
            set outputTxt to "Default Apache stopped OK"
        end if
        
        set outputTxt to do shell script "sudo launchctl unload -w /Library/LaunchDaemons/pete.mxcl.httpd.plist 2>/dev/null" with administrator privileges
        if ((outputTxt as string) is equal to "") then
            set outputTxt to "Apache stopped OK"
        end if
        append_to_output(theOutput,outputTxt)
        set outputTxt to do shell script "export PATH=/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:$PATH; brew services stop mysql@5.7 || echo 'unable to stop mysql'"
        append_to_output(theOutput,outputTxt)
        
        set outputTxt to do shell script "sudo launchctl load -w /System/Library/LaunchDaemons/org.apache.httpd.plist && sudo /usr/sbin/apachectl start" with administrator privileges
        if ((outputTxt as string) is equal to "") then
            set outputTxt to "Default Apache started OK"
        else
            append_to_output(theOutput,outputTxt)
        end if
        
        set outputTxt to do shell script "export PATH=/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:$PATH; sudo brew services stop dnsmasq" with administrator privileges
        append_to_output(theOutput,outputTxt)
        
        set theApacheSw's stringValue to "Off"
        set theDatabaseSw's stringValue to "Off"
        set theDnsmasqSw's stringValue to "Off"
        
    end buttonStop_
    
    on selectPHP_(sender)
        set textValue to theComboBox's stringValue
        log textValue
        
        if ((textValue as string) is equal to "php 5.6") then
            set outputTxt to do shell script "export PATH=/usr/local/bin:/usr/local/sbin:$PATH ;sphp 5.6 || echo 'PHP switch error'"
        end if
        
        if ((textValue as string) is equal to "php 7.0") then
            set outputTxt to do shell script "export PATH=/usr/local/bin:/usr/local/sbin:$PATH ;sphp 7.0 || echo 'PHP switch error'"
        end if
        
        if ((textValue as string) is equal to "php 7.1") then
            set outputTxt to do shell script "export PATH=/usr/local/bin:/usr/local/sbin:$PATH ;sphp 7.1 || echo 'PHP switch error'"
        end if
        
        if ((textValue as string) is equal to "php 7.2") then
            #display alert "Hello again" & textValue & "|"
            set outputTxt to do shell script "export PATH=/usr/local/bin:/usr/local/sbin:$PATH ;sphp 7.2 || echo 'PHP switch error'"
        end if

        if ((textValue as string) is equal to "php 7.3") then
            #display alert "Hello again" & textValue & "|"
            set outputTxt to do shell script "export PATH=/usr/local/bin:/usr/local/sbin:$PATH ;sphp 7.3 || echo 'PHP switch error'"
        end if

        if ((textValue as string) is equal to "php 7.4") then
            #display alert "Hello again" & textValue & "|"
            set outputTxt to do shell script "export PATH=/usr/local/bin:/usr/local/sbin:$PATH ;sphp 7.4 || echo 'PHP switch error'"
        end if
        
        if ((textValue as string) is not equal to "Change php version") then
            append_to_output(theOutput,outputTxt)
        end if
        
        set phpv to do shell script "export PATH=/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:$PATH; php -r 'echo phpversion();' || echo 'Off'"
        set thePHPversion's stringValue to phpv
        
    end selectPHP_
	
end script
